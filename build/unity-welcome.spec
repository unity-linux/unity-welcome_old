Name:           unity-welcome
Version:        29.1
Release:        1%{?dist}
Summary:        Unity-Linux welcome utility

License:        GPLv2+
URL:            http://unityproject.org
Source0:        %{name}-%{version}.tar.xz

BuildArch:      noarch
BuildRequires:  desktop-file-utils
Requires:       python3-dnf
Requires:       python3-lens >= 0.15.0

%description
The Unity-Linux Welcome utility provides a simple interface for accessing all
the relevant information for a new user of Unity-Linux.

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}%{_datadir}/%{name}
mkdir -p %{buildroot}%{_datadir}/icons/hicolor
mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}/etc/skel/.config/autostart

cp -a data/* %{buildroot}%{_datadir}/%{name}
cp -a icons/* %{buildroot}%{_datadir}/icons/hicolor/
install -p -m 644 welcome.desktop %{buildroot}%{_datadir}/applications/welcome.desktop
install -p -m 644 welcome.desktop %{buildroot}/etc/skel/.config/autostart/welcome.desktop
install -p -m 755 welcome %{buildroot}%{_bindir}/welcome

# validate desktop file
desktop-file-validate %{buildroot}%{_datadir}/applications/welcome.desktop

%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%{_bindir}/welcome
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/applications/welcome.desktop
/etc/skel/.config/autostart/welcome.desktop

%changelog
* Mon Jan 21 2019 JMiahMan <JMiahMan@unity-linux.org> - 29.1-1
- Fork from Korora
